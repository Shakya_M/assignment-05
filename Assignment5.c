#include <stdio.h>
/* This programme will determine the exact relationship between the profit and the ticket price which will help the owner to determine the price which he can make the highest profit */

//function the calculate the profit earned
int theprofit(int price);

//fuction to calculate the revenue
int revenue(int price);

//function to calculate the attendance with respective to the ticket price
int attendance(int price);

//function to calculate the expenditure incurred by the owner
int expenditure(int price);

//function to display the relationship table
void display();

int attendance(int price){
	const int att = 120;
	return att - ((price-15) / 5*20);
	
}
 int revenue(int price){
 	return price * attendance(price);
 	
 }
 int expenditure(int price){
 	const int x = 500;
 	return x + 3 * attendance(price);
 	
 }
 int theprofit(int price){
 	return revenue(price)-expenditure(price);
 	
 }
 void display(){
 	int i = 5,profit;
 	for (i=5;i<=50;i+=5){
 		profit= theprofit(i);
 		printf("Rs.%d \t Rs.%d\n",i,profit);
 		
	}
 }
 int main()
 {
 	printf("This table displays the relationship between the profit and the ticket price\n");
 	printf("Price \t Profit\n");
 	display();
 	printf("According to the above table, profit increases at a decreasing rate in the range of Rs.5 to Rs.25 of ticket price and it reaches a maximum profit of Rs.1260/= at the price of Rs.25/= and the profit gradually decreases after the price of Rs.25/=\n");
 	printf("Therefore the owner can make the highest profit at the price of Rs.25/=\n");
 	return 0;
 }
